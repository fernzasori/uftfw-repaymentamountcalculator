﻿iRowCount = Datatable.getSheet("TC002_CAL_RepaymentAmount [TC002_CAL_RepaymentAmount]").getRowCount

tabNameData = Trim((DataTable("TabName","TC002_CAL_RepaymentAmount [TC002_CAL_RepaymentAmount]")))
loanAmount = Trim((DataTable("LoanAmount","TC002_CAL_RepaymentAmount [TC002_CAL_RepaymentAmount]")))
interestRate = Trim((DataTable("InterestRate","TC002_CAL_RepaymentAmount [TC002_CAL_RepaymentAmount]")))
loanTerm = Trim((DataTable("LoanTerm","TC002_CAL_RepaymentAmount [TC002_CAL_RepaymentAmount]")))
mountlyRepayment = Trim((DataTable("MountlyRepayment","TC002_CAL_RepaymentAmount [TC002_CAL_RepaymentAmount]")))



Call FW_OpenWebBrowser("เปิดเวปกรุงศรีเครื่องมือคำนวณ","https://www.krungsri.com/bank/th/Other/Calculator.html","CHROME")
Call FW_ValidateElementMessage("ตรวจสอบแถบเครื่องมือคำนวณ","เครื่องมือคำนวณ l ธนาคารกรุงศร","เครื่องมือคำนวณ l ธนาคารกรุงศร","เครื่องมือคำนวณ","เครื่องมือคำนวณ")
Call FW_ValidateElementMessage("ตรวจสอบแถบเครื่องคำนวณสินเชื่อบ้าน","เครื่องมือคำนวณ l ธนาคารกรุงศร","เครื่องมือคำนวณ l ธนาคารกรุงศร","เครื่องคำนวณสินเชื่อบ้าน","เครื่องคำนวณสินเชื่อบ้าน")
Call FW_Link ("กดเลือกเมนูคำนวณยอดผ่อนชำระต่อเดือน","เครื่องมือคำนวณ l ธนาคารกรุงศร","เครื่องมือคำนวณ l ธนาคารกรุงศร","คำนวณยอดผ่อนชำระต่อเดือน")
Call FW_WebEditAndVerifyResult("กรอกวงเงินที่ขอกู้","คำนวณความสามารถในการผ่อนชำระต่","คำนวณความสามารถในการผ่อนชำระต่","loanAmount",loanAmount,loanAmount)
Call FW_WebEditAndVerifyResult("กรอกอัตราดอกเบี้ย","คำนวณความสามารถในการผ่อนชำระต่","คำนวณความสามารถในการผ่อนชำระต่","interestRate",interestRate,interestRate)
Call FW_WebEditAndVerifyResult("กรอกระยะเวลาที่ขอกู้(ปี)","คำนวณความสามารถในการผ่อนชำระต่","คำนวณความสามารถในการผ่อนชำระต่","loanTerm",loanTerm,loanTerm)
Call FW_WebButton("กดปุ่มเริ่มคำนวน","คำนวณความสามารถในการผ่อนชำระต่","คำนวณความสามารถในการผ่อนชำระต่","เริ่มคำนวณ")
Call FW_ValidateElementMessage("ตรวจสอบยอดเงินผ่อนชำระต่อเดือน","คำนวณความสามารถในการผ่อนชำระต่","คำนวณความสามารถในการผ่อนชำระต่","ยอดผ่อนชำระต่อเดือน",mountlyRepayment)
Call FW_CloseWebBrowserChrome("ปิดเวปกรุงศรีเครื่องมือคำนวณ")

''Browser("เครื่องมือคำนวณ l ธนาคารกรุงศร").Page("เครื่องมือคำนวณ l ธนาคารกรุงศร").Link("คำนวณยอดผ่อนชำระต่อเดือน").Click
'Browser("คำนวณความสามารถในการผ่อนชำระต่").Page("คำนวณความสามารถในการผ่อนชำระต่").WebEdit("loanAmount").Set
'Browser("คำนวณความสามารถในการผ่อนชำระต่").Page("คำนวณความสามารถในการผ่อนชำระต่").WebEdit("interestRate").Set
'Browser("คำนวณความสามารถในการผ่อนชำระต่").Page("คำนวณความสามารถในการผ่อนชำระต่").WebEdit("loanTerm").Set
'
'Browser("คำนวณความสามารถในการผ่อนชำระต่").Page("คำนวณความสามารถในการผ่อนชำระต่").WebButton("เริ่มคำนวณ").Click
'Browser("คำนวณความสามารถในการผ่อนชำระต่").Page("คำนวณความสามารถในการผ่อนชำระต่").WebElement("ยอดผ่อนชำระต่อเดือน").Click


'Browser("เครื่องมือคำนวณ l ธนาคารกรุงศร").Page("เครื่องมือคำนวณ l ธนาคารกรุงศร").WebElement("เครื่องมือคำนวณ").Click
'Browser("เครื่องมือคำนวณ l ธนาคารกรุงศร").Page("เครื่องมือคำนวณ l ธนาคารกรุงศร").WebElement("เครื่องคำนวณสินเชื่อบ้าน").Click
'Browser("เครื่องมือคำนวณ l ธนาคารกรุงศร").Page("เครื่องมือคำนวณ l ธนาคารกรุงศร").Link("คำนวณความสามารถในการกู้").Click
'Browser("คำนวณความสามารถในการกู้-สินเชื").Page("คำนวณความสามารถในการกู้-สินเชื").WebEdit("รายได้").Set
'Browser("คำนวณความสามารถในการกู้-สินเชื").Page("คำนวณความสามารถในการกู้-สินเชื").WebEdit("ภาระหนี้").Set
'Browser("คำนวณความสามารถในการกู้-สินเชื").Page("คำนวณความสามารถในการกู้-สินเชื").WebEdit("ระยะเวลากู้").Set
'Browser("คำนวณความสามารถในการกู้-สินเชื").Page("คำนวณความสามารถในการกู้-สินเชื").WebButton("เริ่มคำนวณ").Click
'Browser("คำนวณความสามารถในการกู้-สินเชื").Page("คำนวณความสามารถในการกู้-สินเชื").WebElement("ท่านสามารถกู้ได้").Click
'Browser("คำนวณความสามารถในการกู้-สินเชื").Page("คำนวณความสามารถในการกู้-สินเชื").WebElement("ยอดผ่อนชำระต่อเดือน").Click


'-----------------
''Call FW_ValidateElementMessage("ตรวจสอบแถบคำนวณจากราคารถยนต์","กรุงศรี มาร์เก็ต รถมือสอง","กรุงศรี มาร์เก็ต รถมือสอง","คำนวณจากราคารถยนต์", "คำนวณจากราคารถยนต์")
''Call FW_WebTabStripAndVerifyElementResult("เลือกคำนวณจากราคารถยนต์","กรุงศรี มาร์เก็ต รถมือสอง","กรุงศรี มาร์เก็ต รถมือสอง","คำนวณจากราคารถยนต์", "ถ้าจะซื้อรถราคาเท่านี้", tabNameData, expectedMessageData)
'Call FW_WebListAndVerifyResult("เลือกประเภทรถ","กรุงศรี มาร์เก็ต รถมือสอง","กรุงศรี มาร์เก็ต รถมือสอง","CarTypeId", carTypeIdData, expectedCarTypeIdData)
'Call FW_WebListAndVerifyResult("เลือกยี่ห้อรถ","กรุงศรี มาร์เก็ต รถมือสอง","กรุงศรี มาร์เก็ต รถมือสอง","CarMakeId", carMakeIdData, expectedCarMakeIdData)
'Call FW_WebListAndVerifyResult("เลือกรุ่นรถ","กรุงศรี มาร์เก็ต รถมือสอง","กรุงศรี มาร์เก็ต รถมือสอง","CarModelId", carModelIdData, expectedCarModelIdData)
'Call FW_WebListAndVerifyResult("เลือกปีรถ","กรุงศรี มาร์เก็ต รถมือสอง","กรุงศรี มาร์เก็ต รถมือสอง","Year", yearData, expectedYearData)
'Call FW_WebEditAndVerifyResult("กรอกราคารถ(ไม่รวมVAT)","กรุงศรี มาร์เก็ต รถมือสอง","กรุงศรี มาร์เก็ต รถมือสอง","ราคารถ(ไม่รวมVAT)", carPriceNoVATData, expectedCarPriceNoVATData)
'Call FW_WebEditAndVerifyResult("กรอกเงินดาวน์","กรุงศรี มาร์เก็ต รถมือสอง","กรุงศรี มาร์เก็ต รถมือสอง","เงินดาวน์", downPaymentData, expectedDownPaymentData)
'Call FW_ValidateEditMessage("ตรวจสอบยอดเงินดาวน์","กรุงศรี มาร์เก็ต รถมือสอง","กรุงศรี มาร์เก็ต รถมือสอง","เงินดาวน์บาท", expectedDownPaymentBahtData)
'Call FW_ValidateEditMessage("ตรวจสอบยอดจัดเช่าซื้อ","กรุงศรี มาร์เก็ต รถมือสอง","กรุงศรี มาร์เก็ต รถมือสอง","ยอดจัดเช่าซื้อ", expectedHirePurchaseData)
'Call FW_WebButtonAndVerifyElementResult("กดปุ่มคำนวนสินเชื่อ","กรุงศรี มาร์เก็ต รถมือสอง","กรุงศรี มาร์เก็ต รถมือสอง","คำนวนสินเชื่อรถ","อัตราผ่อนชำระ(บาทต่อเดือน)",expectedPriceMonthData)

