﻿iRowCount = Datatable.getSheet("TC00_CAL_BorrowingPower [TC00_CAL_BorrowingPower]").getRowCount

tabNameData = Trim((DataTable("TabName","TC00_CAL_BorrowingPower [TC00_CAL_BorrowingPower]")))
monthlyIncome = Trim((DataTable("MonthlyIncome","TC00_CAL_BorrowingPower [TC00_CAL_BorrowingPower]")))
liability = Trim((DataTable("Liability","TC00_CAL_BorrowingPower [TC00_CAL_BorrowingPower]")))
loanTerm = Trim((DataTable("LoanTerm","TC00_CAL_BorrowingPower [TC00_CAL_BorrowingPower]")))
maximumLoanAmount = Trim((DataTable("MaximumLoanAmount","TC00_CAL_BorrowingPower [TC00_CAL_BorrowingPower]")))
mountlyRepayment = Trim((DataTable("MountlyRepayment","TC00_CAL_BorrowingPower [TC00_CAL_BorrowingPower]")))
expectedmonthly = Trim((DataTable("Expectedmonthly","TC00_CAL_BorrowingPower [TC00_CAL_BorrowingPower]")))


Call FW_OpenWebBrowser("เปิดเวปกรุงศรีเครื่องมือคำนวณ","https://www.krungsri.com/bank/th/Other/Calculator.html","CHROME")
Call FW_ValidateElementMessage("ตรวจสอบแถบเครื่องมือคำนวณ","เครื่องมือคำนวณ l ธนาคารกรุงศร","เครื่องมือคำนวณ l ธนาคารกรุงศร","เครื่องมือคำนวณ","เครื่องมือคำนวณ")
Call FW_ValidateElementMessage("ตรวจสอบแถบเครื่องคำนวณสินเชื่อบ้าน","เครื่องมือคำนวณ l ธนาคารกรุงศร","เครื่องมือคำนวณ l ธนาคารกรุงศร","เครื่องคำนวณสินเชื่อบ้าน","เครื่องคำนวณสินเชื่อบ้าน")
Call FW_Link ("กดเลือกเมนูคำนวณความสามารถในการกู้","เครื่องมือคำนวณ l ธนาคารกรุงศร","เครื่องมือคำนวณ l ธนาคารกรุงศร","คำนวณความสามารถในการกู้")
Call FW_WebEditAndVerifyResult("กรอกรายได้ต่อเดือน","คำนวณความสามารถในการกู้-สินเชื","คำนวณความสามารถในการกู้-สินเชื","รายได้",monthlyIncome,expectedmonthly)
Call FW_WebEditAndVerifyResult("กรอกภาระหนี้ต่อเดือน","คำนวณความสามารถในการกู้-สินเชื","คำนวณความสามารถในการกู้-สินเชื","ภาระหนี้",liability,liability)
Call FW_WebEditAndVerifyResult("กรอกระยะเวลาที่ขอกู้(ปี)","คำนวณความสามารถในการกู้-สินเชื","คำนวณความสามารถในการกู้-สินเชื","ระยะเวลากู้",loanTerm,loanTerm)
Call FW_WebButton("กดปุ่มเริ่มคำนวน","คำนวณความสามารถในการกู้-สินเชื","คำนวณความสามารถในการกู้-สินเชื","เริ่มคำนวณ")
Call FW_ValidateElementMessage("ตรวจสอบยอดเงินที่ท่านสามารถกู้ได้","คำนวณความสามารถในการกู้-สินเชื","คำนวณความสามารถในการกู้-สินเชื","ท่านสามารถกู้ได้",maximumLoanAmount)
Call FW_ValidateElementMessage("ตรวจสอบยอดเงินผ่อนชำระต่อเดือน","คำนวณความสามารถในการกู้-สินเชื","คำนวณความสามารถในการกู้-สินเชื","ยอดผ่อนชำระต่อเดือน",mountlyRepayment)
Call FW_CloseWebBrowserChrome("ปิดเวปกรุงศรีเครื่องมือคำนวณ")

'Browser("เครื่องมือคำนวณ l ธนาคารกรุงศร").Page("เครื่องมือคำนวณ l ธนาคารกรุงศร").WebElement("เครื่องมือคำนวณ").Click
'Browser("เครื่องมือคำนวณ l ธนาคารกรุงศร").Page("เครื่องมือคำนวณ l ธนาคารกรุงศร").WebElement("เครื่องคำนวณสินเชื่อบ้าน").Click
'Browser("เครื่องมือคำนวณ l ธนาคารกรุงศร").Page("เครื่องมือคำนวณ l ธนาคารกรุงศร").Link("คำนวณความสามารถในการกู้").Click
'Browser("คำนวณความสามารถในการกู้-สินเชื").Page("คำนวณความสามารถในการกู้-สินเชื").WebEdit("รายได้").Set
'Browser("คำนวณความสามารถในการกู้-สินเชื").Page("คำนวณความสามารถในการกู้-สินเชื").WebEdit("ภาระหนี้").Set
'Browser("คำนวณความสามารถในการกู้-สินเชื").Page("คำนวณความสามารถในการกู้-สินเชื").WebEdit("ระยะเวลากู้").Set
'Browser("คำนวณความสามารถในการกู้-สินเชื").Page("คำนวณความสามารถในการกู้-สินเชื").WebButton("เริ่มคำนวณ").Click
'Browser("คำนวณความสามารถในการกู้-สินเชื").Page("คำนวณความสามารถในการกู้-สินเชื").WebElement("ท่านสามารถกู้ได้").Click
'Browser("คำนวณความสามารถในการกู้-สินเชื").Page("คำนวณความสามารถในการกู้-สินเชื").WebElement("ยอดผ่อนชำระต่อเดือน").Click


'-----------------
''Call FW_ValidateElementMessage("ตรวจสอบแถบคำนวณจากราคารถยนต์","กรุงศรี มาร์เก็ต รถมือสอง","กรุงศรี มาร์เก็ต รถมือสอง","คำนวณจากราคารถยนต์", "คำนวณจากราคารถยนต์")
''Call FW_WebTabStripAndVerifyElementResult("เลือกคำนวณจากราคารถยนต์","กรุงศรี มาร์เก็ต รถมือสอง","กรุงศรี มาร์เก็ต รถมือสอง","คำนวณจากราคารถยนต์", "ถ้าจะซื้อรถราคาเท่านี้", tabNameData, expectedMessageData)
'Call FW_WebListAndVerifyResult("เลือกประเภทรถ","กรุงศรี มาร์เก็ต รถมือสอง","กรุงศรี มาร์เก็ต รถมือสอง","CarTypeId", carTypeIdData, expectedCarTypeIdData)
'Call FW_WebListAndVerifyResult("เลือกยี่ห้อรถ","กรุงศรี มาร์เก็ต รถมือสอง","กรุงศรี มาร์เก็ต รถมือสอง","CarMakeId", carMakeIdData, expectedCarMakeIdData)
'Call FW_WebListAndVerifyResult("เลือกรุ่นรถ","กรุงศรี มาร์เก็ต รถมือสอง","กรุงศรี มาร์เก็ต รถมือสอง","CarModelId", carModelIdData, expectedCarModelIdData)
'Call FW_WebListAndVerifyResult("เลือกปีรถ","กรุงศรี มาร์เก็ต รถมือสอง","กรุงศรี มาร์เก็ต รถมือสอง","Year", yearData, expectedYearData)
'Call FW_WebEditAndVerifyResult("กรอกราคารถ(ไม่รวมVAT)","กรุงศรี มาร์เก็ต รถมือสอง","กรุงศรี มาร์เก็ต รถมือสอง","ราคารถ(ไม่รวมVAT)", carPriceNoVATData, expectedCarPriceNoVATData)
'Call FW_WebEditAndVerifyResult("กรอกเงินดาวน์","กรุงศรี มาร์เก็ต รถมือสอง","กรุงศรี มาร์เก็ต รถมือสอง","เงินดาวน์", downPaymentData, expectedDownPaymentData)
'Call FW_ValidateEditMessage("ตรวจสอบยอดเงินดาวน์","กรุงศรี มาร์เก็ต รถมือสอง","กรุงศรี มาร์เก็ต รถมือสอง","เงินดาวน์บาท", expectedDownPaymentBahtData)
'Call FW_ValidateEditMessage("ตรวจสอบยอดจัดเช่าซื้อ","กรุงศรี มาร์เก็ต รถมือสอง","กรุงศรี มาร์เก็ต รถมือสอง","ยอดจัดเช่าซื้อ", expectedHirePurchaseData)
'Call FW_WebButtonAndVerifyElementResult("กดปุ่มคำนวนสินเชื่อ","กรุงศรี มาร์เก็ต รถมือสอง","กรุงศรี มาร์เก็ต รถมือสอง","คำนวนสินเชื่อรถ","อัตราผ่อนชำระ(บาทต่อเดือน)",expectedPriceMonthData)

